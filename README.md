# Helix Geonames Task

### Installation
First time you must be clone the repository:
```
git clone https://k3rnel7@bitbucket.org/k3rnel7/helix-geonames-task.git
```
After cloning you must install composer dependencies by running:
```
composer install
```
**After dependencies installation, copy `.env.example` to `.env` and configure it for your environment:**

Next, you need to migrate your database, just run:
```
php artisan migrate
```
For database seeding you need to run:
```
php artisan db:seed
```

If you want to start GeoNames migration just run (you can change country if you need)
```
php artisan geonames:import --country=RU
```

Or you can just customize your task scheduler and cities will be imported or updated.

## Thats all :)