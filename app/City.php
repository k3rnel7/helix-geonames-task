<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 * @package App
 */
class City extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'city_id',
        'name',
        'ascii_name',
        'alternate_names',
        'latitude',
        'longitude',
        'f_class',
        'f_code',
        'country_id',
        'cc2',
        'admin1',
        'admin2',
        'admin3',
        'admin4',
        'population',
        'elevation',
        'gtopo30',
        'timezone_id',
        'modification_at',
    ];
}
