<?php

namespace App\Console\Commands;

use App\Services\GeoNames;
use Illuminate\Console\Command;

/**
 * Class ImportGeonames
 * @package App\Console\Commands
 */
class ImportGeonames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geonames:import {--country=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import geonames';

    /**
     * @var GeoNames
     */
    protected $geoNamesService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->geoNamesService = new GeoNames(env('GEONAMES_DEVELOPMENT_MODE', false));

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Cities importing started');

        $country = $this->input->getOption('country');

        if (is_null($country)) {
            $this->warn('--country option is required');
            return 1;
        }

        $result = $this->geoNamesService->import($country);

        if ($result) {
            $this->info('Cities importing successfully completed');
        } else {
            $this->info('The cities of a given country do not need updating');
        }

        return true;
    }
}
