<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountriesUpdates extends Model
{
    public $timestamps = [
        'last_modified'
    ];

    protected $fillable = [
        'country',
        'last_modified'
    ];
}
