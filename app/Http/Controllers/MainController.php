<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchCities(Request $request)
    {
        $data = City::query()
            ->select(['city_id', 'name'])
            ->where('name', 'LIKE', '%' . $request->query('q') . '%')
            ->get();

        return response()->json($data);
    }

    /**
     * @param $id
     * @return array
     */
    public function getNearestCities($id)
    {
        $city = City::query()->where('city_id', $id)->firstOrFail();

        return DB::select("SELECT `city_id`, `name`, `latitude`, `longitude`, (3959 * ACOS(COS(RADIANS(?)) * COS(RADIANS(latitude)) * COS( RADIANS(longitude) - RADIANS(?)) + SIN(RADIANS(?)) * SIN(RADIANS(latitude)))) AS distance FROM cities ORDER BY distance LIMIT 0 , 20", [$city->latitude, $city->longitude, $city->latitude]);
    }
}
