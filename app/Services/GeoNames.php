<?php

namespace App\Services;

use App\City;
use App\CountriesUpdates;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use RuntimeException;
use ZipArchive;

/**
 * Class GeoNames
 * @package App\Services
 */
class GeoNames
{
    /**
     * File archive instance.
     *
     * @var ZipArchive
     */
    protected $archive;

    /**
     * @var mixed
     */
    protected $url;

    /**
     * @var \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    protected $config;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Builder
     */
    protected $citiesModelBuilder;

    /**
     * @var Builder
     */
    protected $countriesUpdatesModelBuilder;

    /**
     * GeoNames constructor.
     * @param bool $developmentMode
     */
    public function __construct($developmentMode = false)
    {
        $this->config = config('geonames');
        $this->url = $developmentMode ? $this->config['urls']['zip'] : $this->config['urls']['dump'];
        $this->archive = new ZipArchive();
        $this->filesystem = Storage::disk('geonames');
        $this->citiesModelBuilder = City::query();
        $this->countriesUpdatesModelBuilder = CountriesUpdates::query();
    }

    /**
     * @param string $country
     * @return bool
     */
    public function import(string $country)
    {
        if (!$this->downloadFile($country)) {
            return false;
        }

        $filename = $this->extractZip($this->filesystem->path('dumps'), $country . '.zip');

        $this->processImport($this->filesystem->path("dumps/$filename"));

        $this->deleteFile($filename);

        return true;
    }

    /**
     * @param $country
     */
    protected function downloadFile(string $country)
    {
        $response = Http::get(sprintf($this->url, $country));

        $countryModel = $this->getCountryModel($country);

        $sourceLastModified = Carbon::make($response->header('Last-Modified'));

        if (is_null($countryModel->last_modified) || $sourceLastModified->greaterThan($countryModel->last_modified)) {
            $countryModel->last_modified = $sourceLastModified;

            $countryModel->save();
        } else {
            return false;
        }

        $file = $response->body();

        $this->filesystem->put("dumps/$country.zip", $file);

        return true;
    }

    /**
     * @param string $country
     * @return CountriesUpdates|Builder
     */
    protected function getCountryModel(string $country): CountriesUpdates
    {
        return $this->countriesUpdatesModelBuilder
            ->firstOrCreate(
                [
                    'country' => $country
                ],
                [
                    'last_modified' => null
                ]
            );
    }

    /**
     * @param $path
     * @param $filename
     * @return string
     */
    protected function extractZip(string $path, string $filename): string
    {
        $this->archive->open($path . '/' . $filename);

        $this->archive->extractTo($path);
        $this->archive->close();

        $this->filesystem->delete(['dumps/' . $filename, 'dumps/readme.txt']);

        return str_replace('.zip', '.txt', $filename);
    }

    /**
     * @param string $path
     */
    protected function processImport(string $path)
    {
        $this->parseFile($path, function ($row) {
            $insertData = [
                'city_id' => $row[0],
                'ascii_name' => $row[2],
                'alternate_names' => $row[3],
                'latitude' => $row[4],
                'longitude' => $row[5],
                'f_class' => $row[6],
                'f_code' => $row[7],
                'country_id' => $row[8],
                'cc2' => $row[9],
                'admin1' => $row[10],
                'admin2' => $row[11],
                'admin3' => $row[12],
                'admin4' => $row[13],
                'population' => $row[14],
                'elevation' => $row[15] ?: null,
                'gtopo30' => $row[16],
                'timezone_id' => $row[17],
                'modification_at' => $row[18],
            ];

            $this->citiesModelBuilder->updateOrCreate([
                'name' => $row[1],
            ], $insertData);
        });
    }

    /**
     * @param $path
     * @param callable $callback
     */
    protected function parseFile(string $path, callable $callback)
    {
        $handle = fopen($path, 'r');

        if (!$handle) {
            throw new RuntimeException("Impossible to open file: $path");
        }

        while (!feof($handle)) {
            $line = fgets($handle, 1024 * 32);

            if (!$line or $line === '' or strpos($line, '#') === 0) continue;

            $line = explode("\t", $line);

            if (!isset($line[7]) || $line[7] !== 'PPL') continue;

            $callback($line);
        }

        fclose($handle);
    }

    /**
     * @param string $filename
     */
    protected function deleteFile(string $filename)
    {
        $this->filesystem->delete("dumps/$filename");
    }
}
