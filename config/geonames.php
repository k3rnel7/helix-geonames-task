<?php

return [
    'urls' => [
        'dump' => 'http://download.geonames.org/export/dump/%s.zip',
        'zip' => 'http://download.geonames.org/export/zip/%s.zip'
    ],
];
