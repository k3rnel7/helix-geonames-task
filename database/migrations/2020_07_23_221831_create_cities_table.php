<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->integer('city_id');
            $table->string('name', 200);
            $table->string('ascii_name', 200);
            $table->string('alternate_names', 4000);
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->string('f_class', 1);
            $table->string('f_code', 10);
            $table->string('country_id', 2)->index();
            $table->string('cc2', 60);
            $table->string('admin1', 20)->index();
            $table->string('admin2', 80)->index();
            $table->string('admin3', 20)->index();
            $table->string('admin4', 20)->index();
            $table->integer('population')->index();
            $table->integer('elevation')->nullable();
            $table->integer('gtopo30')->nullable();
            $table->string('timezone_id', 40)->index();
            $table->date('modification_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
