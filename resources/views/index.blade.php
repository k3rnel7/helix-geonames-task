<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cities search</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .twitter-typeahead {
            display: block;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Cities search</h1>
            <hr>
            <div class="form-group">
                <input style="width: 100% !important;" id="search-input" type="search" name="name" class="form-control"
                       placeholder="Type city name"
                       autocomplete="off">
                <input id="selected-city" type="hidden" name="city_id">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="map-container" style="height: 500px;">
                <div class="map-title">
                    <h5>Google Maps</h5>
                </div>
                <div class="map-wrapper">
                    <div class="google-map" id="map" style="height: 600px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins and Typeahead) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- Typeahead.js Bundle -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}" type="text/javascript"></script>

<script>
    $(document).ready(function ($) {
        const nearestCitiesRoute = '{{ route('nearestCities', ['id' => ':slug']) }}'

        let engine = new Bloodhound({
            remote: {
                url: '{{ route('search-cities') }}?q=%QUERY%',
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

        $("#search-input").typeahead({
            hint: true,
            highlight: true,
            minLength: 2,
        }, {
            source: engine.ttAdapter(),

            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            name: 'citiesList',
            displayKey: 'name',
            // the key from the array we want to display (name,id,email,etc...)
            templates: {
                empty: [
                    '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                ],
                header: [
                    '<div class="list-group search-results-dropdown">'
                ],
                suggestion: function (data) {
                    return '<span class="list-group-item" style="display: block">' + data.name + '</span>'
                }
            }
        });

        $('#search-input').on('typeahead:selected', function (e, item) {
            $('#selected-city').val(item.city_id);

            $.getJSON(nearestCitiesRoute.replace(':slug', item.city_id), function (response) {
                google.maps.event.addDomListener(window, 'load', initGoogleMaps(response));
            });
        });
    });

    google.maps.event.addDomListener(window, 'load', initGoogleMaps());

    function initGoogleMaps(locations = []) {
        let map = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            // Coordinates of  Russia
            center: new google.maps.LatLng(55, 37)
        });

        addMarker(locations, map);
    }

    function addMarker(locations, map) {
        let marker;

        for (let i = 0; i < locations.length; i++) {
            if (i === 0) {
                map.setCenter(new google.maps.LatLng(parseInt(locations[i].latitude), parseInt(locations[i].longitude)))
            }

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                label: locations[i].name,
                map: map
            });
        }

        if (locations[0] !== undefined) {
            map.panTo(new google.maps.LatLng(locations[0].latitude, locations[0].longitude));
        }
    }
</script>
</body>
</html>
